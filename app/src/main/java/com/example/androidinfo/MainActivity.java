package com.example.androidinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String ModelNumber, Board, Brand, Display, FingerPrint, ID, TAGS, Type, AndroidVersion, APILevel, CodeName, INCREMENTAL;
    String Kernel, SecurityPatch;
    TextView text1, text2;
    TelephonyManager tel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ModelNumber = android.os.Build.MODEL;
        Board = android.os.Build.BOARD;
        Brand = android.os.Build.BRAND;
        Display = android.os.Build.DISPLAY;
        FingerPrint = android.os.Build.FINGERPRINT;
        ID = android.os.Build.ID;
        TAGS = android.os.Build.TAGS;
        Type = android.os.Build.TYPE;

        AndroidVersion = android.os.Build.VERSION.RELEASE;
        APILevel = android.os.Build.VERSION.SDK;
        CodeName = android.os.Build.VERSION.CODENAME;
        //INCREMENTAL = android.os.Build.VERSION.INCREMENTAL;
        SecurityPatch = Build.VERSION.SECURITY_PATCH;

        Kernel = System.getProperty("os.version");

        text1 = (TextView) findViewById(R.id.deviceInfo);
        text2 = (TextView) findViewById(R.id.simInfo);
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        /*
        text.setText("Operator Code : " + tel.getSimOperator().toString()
                + "\nOperator Name : " + tel.getSimOperatorName().toString()
                + "\nCountry ISO : " + tel.getSimCountryIso().toString());


        text.setText(Html.fromHtml("Phone Type" +
                "<br/><br/><font color = 'red';>Model Number : </font></font>" + ModelNumber
                + "<br/><font color = 'red';>Board : </font>" + Board
                + "<br/><font color = 'red';>Brand : </font>" + Brand
                + "<br/><font color = 'red';>Display : </font>" + Display
                + "<br/><font color = 'red';>FingerPrint : </font>" + FingerPrint
                + "<br/><font color = 'red';>ID : </font>" + ID
                + "<br/><font color = 'red';>TAGS : </font>" + TAGS
                + "<br/><font color = 'red';>Type : </font>" + Type

                + "<br/>"

                + "<br/><font color = 'red';>Android Version : </font>" + AndroidVersion
                + "<br/><font color = 'red';>API Level : </font>" + APILevel
                + "<br/><font color = 'red';>CodeName : </font>" + CodeName
                + "<br/><font color = 'red';>INCREMENTAL : </font>" + INCREMENTAL));
        */

        // alternative mode
        text1.setText("Model Number : " + ModelNumber
                + "\nBoard : " + Board
                + "\nBrand : " + Brand
                + "\nAndroid Version : " + AndroidVersion
                + "\nAPI Level : " + APILevel
                + "\nCodeName : " + CodeName
                + "\nKernel : " + Kernel
                + "\nSecurityPatch : " + SecurityPatch
                + "\n"
                + "\nBuild Number : " + Display
                + "\nFingerPrint : " + FingerPrint);
        
        // cut
        // + "\nID : " + ID
        //            + "\nTAGS : " + TAGS
        //                + "\nType : " + Type
        //+ "\nINCREMENTAL : " + INCREMENTAL
        String operator_code = tel.getNetworkOperator();

        text2.setText("\nOperator Code : " + tel.getSimOperator().toString()
                + "\nOperator Name : " + tel.getSimOperatorName().toString()
                + "\nCountry ISO : " + tel.getSimCountryIso().toString()
                + "\nNetwork Operator : " + operator_code);
    }
}