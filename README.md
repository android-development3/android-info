# Android Info 

### Method 
```java
ModelNumber = android.os.Build.MODEL;
Board = android.os.Build.BOARD;

// build number 
Brand = android.os.Build.BRAND;
Display = android.os.Build.DISPLAY;
FingerPrint = android.os.Build.FINGERPRINT;
ID = android.os.Build.ID;
TAGS = android.os.Build.TAGS;
Type = android.os.Build.TYPE;

// android version 
AndroidVersion = android.os.Build.VERSION.RELEASE;

// api lavel 
APILevel = android.os.Build.VERSION.SDK;
CodeName = android.os.Build.VERSION.CODENAME;
INCREMENTAL = android.os.Build.VERSION.INCREMENTAL;

// security patch date 
SecurityPatch = Build.VERSION.SECURITY_PATCH;

// to get kernel info
Kernel = System.getProperty("os.version");

// sim information 
tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)
String operator_code = tel.getNetworkOperator();

text2.setText("\nOperator Code : " + tel.getSimOperator().toString()
        + "\nOperator Name : " + tel.getSimOperatorName().toString()
        + "\nCountry ISO : " + tel.getSimCountryIso().toString()
        + "\nNetwork Operator : " + operator_code);
```
### Screenshot 
![title](main_screenshot_2.PNG)

:checkered_flag: [Download APK](https://gitlab.com/android-development3/android-info/-/raw/master/app/release/app-release.apk)



**Development By,** <br>
***:bd: Farhan Sadik***